#### Ludvigs maze solving agent ####

AI that plays trasure hunt; 

A maze solving game involving obstacles like rocks, trees, water and doors. 
The world view is restricted to 5x5 squares around the agent, so it must keep its own internal map view.

The goal is to find the treasure and bring it back home.
If the moves are made in the wrong order the agent fails.

Uses a modified BFS to traverse future states and heuristics to reduce computational complexity.
Solves every maze.
