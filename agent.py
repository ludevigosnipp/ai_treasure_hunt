#!/usr/bin/python3
# ^^ note the python directive on the first line
# COMP 9414 agent initiation file 
# requires the host is running before the agent
# designed for python 3.6
# typical initiation would be (file in working directory, port = 31415)
#        python3 agent.py -p 31415
# created by Leo Hoare
# with slight modifications by Alan Blair

# implemented solution by Ludvig A. Løddesøl, z5026944

import sys
import socket
import time
import subprocess
import random
import numpy as np
from collections import deque, defaultdict
from copy import deepcopy

# if sys.argv[-1] == '9000': # <- coolest port ever
#     print("Opening java.")
#     subprocess.run("nohup java Step -p 9000 -i s5.in &", shell=True)
#     time.sleep(.5)

#static globals
map_bound = (-1, 159)
land = {' ', 'O'}
sea = {'~'}
start_time = time.time()

##################################################################
############################ QUESTION ############################
##################################################################

"""
    How does my program work?
    
    I do a BFS over all available items of interest.
    If they are costly, aka changing the state of the game to something that may be bad,
    I DFS into their "future" by recursively DFSing as if I performed that action
    Each item is handled in a certain way to accommodate for what works for it.
    Check out the discovery, rock and island (for rafts) sections to see.

    What algorithms are used?
    
    DFS, BFS with various tweaks. Many tweaks to serve the decision tree process.
    There are also types of dynamic programming approaches, where I store various outputs
    in order to rule out future testing. Especially in the stepping stone handling this
    proved very useful.

    What data structures are used?
    
    I chose to store an internal KB == map in a maze class, that stores the current look
    of the game. It basically stores everything that has happened so far in a 2d numpy array.
    This allows for relatively fast calculation while being very human comprehensable.
    It rolls the map around everytime the player turns, so its easy for a human to understand.
    
    I also store states, which are future states where alternations has been done.
    Here the map does not get stored, rather just the small things that are tweaked,
    including their impact on the abilities of the agent.

    Inside the BFS I store paths = sequences of moves, and if they are costly I store 
    their state, end location and end direction (up/down/left/right) to use for further DFS.


    What further design decisions did I make?
    
    The design decisions are slightly described above already.
    Other things was the large decision to go with the search approach, rather than NN.
    I reckon this makes it way more human comprehensable, and tought me more about
    the way to design decisions in an AI. And how to structure and "bool-ify" my own thoughts.
    The map storage decision was a hard one and proved several challanges along the way.
    The same with the future states decision. But they worked out rather well!
    
    Same with the decision to split paths in costly and free, and going deeper into the costly
    paths to see if they lead somewhere fun.
    However it does prioritise discovery over anything (just in case), so even if a solution
    is around, if there is free discovery to be made the AI will go all Chris Colombus on 
    the maze.

    Solves all example maps on my computer in < 8 seconds wopwop!

"""

##################################################################
########################## WORLD VIEW ############################
##################################################################

class Maze():
    def __init__(self):
        self.map = np.array([['?' for _ in range(map_bound[1])] for _ in range(map_bound[1])])
        self.home = [79, 79]
        self.player = [79, 79]
        self.no_moves = 0
        self.moves = ['?']
        self.on_land = True

        #items
        self.has_gold = False
        self.has_key = False
        self.has_axe = False
        self.has_raft = False
        self.no_rocks = 0

        # we're on a mission!
        self.mission = []
        self.target = ' '

    def rotate_map_l(self):
        self.map = np.rot90(self.map)

    def rotate_map_r(self):
        self.map = np.rot90(self.map, -1)

    def add_to_map(self, view):
        last_move = self.moves[-1]
        if last_move in ('?', 'f', 'c', 'u'):
            y, x = self.player
            y -= 2
            x -= 2
            for i in range(5):
                for j in range(5):
                    if i == j == 2: 
                        if self.map[i + y, j + x] == 'o':
                            self.map[i + y, j + x] = ' '
                    else:
                        self.map[i + y, j + x] = view[i][j]
        self.map[tuple(self.home)] = 'H'

    def print_map(self):
        to_reduce = deepcopy(self.map)
        to_reduce[tuple(self.player)] = '^'
        to_reduce = np.rot90(to_reduce[(to_reduce != '?').any(axis=1)])
        to_reduce = np.rot90(to_reduce[(to_reduce != '?').any(axis=1)], -1)

        mission_to_show = self.mission[::-1]
        if len(mission_to_show) > 5:
            mission_to_show = mission_to_show[:5] + ['... +%d' % (len(mission_to_show)-5)]

        items = ['$$$: %s' % self.has_gold + '  \tkey: %s' % self.has_key 
                + ' \taxe: %s' % self.has_axe, 
                'raft: %s' % self.has_raft + '\trocks: %s' % self.no_rocks
                + '\ton land: %s' % self.on_land, 
                'moves: %d' % self.no_moves + '\tlast m: %s' % self.moves[-1], 
                'Target: %s' % self.target, 'MISSION: %s' % mission_to_show]
        len_items = len(items)

        width = len(to_reduce[0]) * 2 - 1
        print('X' + '-'*width + 'X', to_reduce.shape, tuple(self.player))
        for i, ln in enumerate(to_reduce):
            print("|%s|" % ' '.join(ln), items[i] if i < len_items else '')
        print('X' + '-'*width + 'X')

    def f(self):
        self.player[0] -= 1
        new_pos = self.map[tuple(self.player)]
        
        if new_pos == 'k':
            self.has_key = True
        elif new_pos == 'o':
            self.no_rocks += 1
        elif new_pos == 'a':
            self.has_axe = True
        elif new_pos == '$':
            self.has_gold = True
        elif new_pos == '~':
            if self.no_rocks > 0:
                self.no_rocks -= 1
            elif self.has_raft or not self.on_land:
                self.has_raft = False
                self.on_land = False
                return #dont wanna be on land!
            else:
                print(self.on_land)
                print('dont walk into water dumbfuck')
                #raise
        elif new_pos == 'H' and self.has_gold:
            print("\nyou just won you beautiful bastard.")
            print("it took %.2f seconds\n" % (time.time() - start_time))
        elif new_pos not in land|{'H'}:
            print('dont walk into %s dumbfuck' % new_pos)
            raise
        self.on_land = True

    def l(self):
        self.rotate_map_r()
        self.player = [self.player[1], map_bound[1] - self.player[0] - 1]
        self.home = [self.home[1], map_bound[1] - self.home[0] - 1]

    def r(self):
        self.rotate_map_l()
        self.player = [map_bound[1] - self.player[1] - 1, self.player[0]]
        self.home = [map_bound[1] - self.home[1] - 1, self.home[0]]

maze = Maze()

##################################################################
######################### FUTURE STATES ##########################
##################################################################

class State():
    # a state refers to the abilities of the agent
    def __init__(self, on_land=True, has_gold=False, has_key=False, 
        has_axe=False, has_raft=False, no_rocks=0, cost_debth=0):
        
        self.on_land = on_land
        self.has_gold = has_gold
        self.has_key = has_key
        self.has_axe = has_axe
        self.has_raft = has_raft
        self.no_rocks = no_rocks
        self.cost_debth = cost_debth

        self.seen = set()
        self.is_now_land = set()
        self.update_terrain()

    def add_item(self, item, new_loc):
        if item == '~':
            if self.no_rocks:
                self.no_rocks -= 1
            elif self.has_raft or not self.on_land:
                self.has_raft = False
                self.on_land = False
                self.update_terrain()
                return
        elif item == 'o':
            self.no_rocks += 1
        elif item == 'T':
            self.has_raft = True
        elif item == 'a':
            self.has_axe = True
        elif item == '$':
            self.has_gold = True
        elif item == 'k':
            self.has_key = True
        if item not in land:
            #picked up items and placed rocks are now land!
            self.is_now_land.add(new_loc)
        if not self.on_land:
            self.on_land = True
            self.has_raft = False
        self.update_terrain()

    def update_terrain(self):
        self.walkable_land = land.copy()
        self.no_cost_items = set()
        self.cost_items = {'o'}|({'T'} if self.has_axe else set())
        (self.no_cost_items if self.has_gold else self.walkable_land).add('H')
        if self.on_land and not self.cost_debth:
            (self.no_cost_items if not self.has_gold else self.walkable_land).add('$')
            (self.no_cost_items if not self.has_axe else self.walkable_land).add('a')
            (self.no_cost_items if not self.has_key else self.walkable_land).add('k')
            if self.has_key:
                self.no_cost_items.add('-')
        else:
            (self.cost_items if not self.has_gold else self.walkable_land).add('$')
            (self.cost_items if not self.has_axe else self.walkable_land).add('a')
            (self.cost_items if not self.has_key else self.walkable_land).add('k')
            if self.has_key:
                self.cost_items.add('-')

        if self.on_land:
            self.walking_tile = self.walkable_land
            self.not_walking_tile = sea
            if self.no_rocks or self.has_raft:
                self.cost_items = self.cost_items|sea
        else:
            self.walking_tile = sea
            self.not_walking_tile = self.walkable_land
            self.cost_items = self.cost_items|self.walkable_land

        self.unwalkable_tile = {'*', '.'}
        if not self.has_key:
            self.unwalkable_tile.add('-')
        if not self.has_axe:
            self.unwalkable_tile.add('T')
        if not self.no_rocks and not self.has_raft and self.on_land:
            self.unwalkable_tile.add('~')

        #this never happens!
        if self.cost_debth and self.no_cost_items - {'H'}:
            print("nocosts", self.no_cost_items)
            raise

    def sum_up_state(self):
        print("---- SUMMING UP STATE ----")
        print("walk on", self.walking_tile)
        print("not walking tile", self.not_walking_tile)
        print("unwalkable", self.unwalkable_tile)
        print("now land", self.is_now_land)
        print('')
        print("nocost", self.no_cost_items)
        print("costly", self.cost_items)
        print('')
        print('on_land', self.on_land)
        print('has_gold', self.has_gold)
        print('has_key', self.has_key)
        print('has_axe', self.has_axe)
        print('has_raft', self.has_raft)
        print('no_rocks', self.no_rocks)
        print('cost_debth', self.cost_debth)
        print('')

##################################################################
###################### The BFS of all BFS ########################
###################### with a hint of DFS ########################
##################################################################

def sort_lists_in_dict(d, key=len):
    return {item: sorted(d[item], key=key) for item in d}

move_dict_generator = {
    'le': lambda y, x: {(y,x-1): [['f'], 'le'], (y+1,x): [['l', 'f'], 'do'], (y-1,x): [['r', 'f'], 'up'], (y,x+1): [['r', 'r', 'f'], 'ri']},
    'do': lambda y, x: {(y+1,x): [['f'], 'do'], (y,x+1): [['l', 'f'], 'ri'], (y,x-1): [['r', 'f'], 'le'], (y-1,x): [['r', 'r', 'f'], 'up']},
    'ri': lambda y, x: {(y,x+1): [['f'], 'ri'], (y-1,x): [['l', 'f'], 'up'], (y+1,x): [['r', 'f'], 'do'], (y,x-1): [['r', 'r', 'f'], 'le']},
    'up': lambda y, x: {(y-1,x): [['f'], 'up'], (y,x-1): [['l', 'f'], 'le'], (y,x+1): [['r', 'f'], 'ri'], (y+1,x): [['r', 'r', 'f'], 'do']}
}
not_rockable = defaultdict(set)
def very_sophisticated_bfs(grid, start_loc, start_state, start_direction='up', path=[]):
    global not_rockable
    path = []
    #at the end of every path, we have a direction, location and state
    start_state.seen.add(start_loc)
    queue = deque([path + [start_direction, start_loc, start_state]])
    reachable = defaultdict(list)
    costly_reachable = defaultdict(list)

    state = start_state
    walking_tile = state.walking_tile
    not_walking_tile = state.not_walking_tile
    no_cost_items = state.no_cost_items
    cost_items = state.cost_items
    cost_debth = state.cost_debth
    on_land = state.on_land
    no_rocks = state.no_rocks
    is_now_land = state.is_now_land

    while queue:
        path = queue.popleft()
        state = path.pop()
        loc = path.pop()
        direction = path.pop()

        move_dict = move_dict_generator[direction](*loc)

        for new_loc in move_dict:
            if new_loc in state.seen or new_loc[0] in map_bound or new_loc[1] in map_bound: continue
            state.seen.add(new_loc)

            moves, new_direction = move_dict[new_loc]
            new_loc_item = grid[new_loc] if new_loc not in is_now_land else ' '
            if new_loc_item in walking_tile:
                queue.append(path + moves + [new_direction, new_loc, state])
                if '?' not in reachable:
                    if not cost_debth and unknown_in_reach(*new_loc):
                        reachable['?'] += [path + moves + [cost_debth]]
                    elif cost_debth and not costly_reachable['?'] and unknown_in_reach(*new_loc) and unknown_bfs(grid, new_loc, walking_tile):
                    #elif cost_debth and not costly_reachable and unknown_bfs(grid, new_loc, walking_tile):
                        costly_reachable['?'] += [path + moves + [new_direction, new_loc, state]]
            
            elif new_loc_item in no_cost_items:
                if new_loc_item == '-':
                    moves.insert(-1, 'u')
                reachable[new_loc_item] += [path + moves + [cost_debth]]
                if not cost_debth:
                    print("NO COST MAH GAHD")
                    return sort_lists_in_dict(reachable)
            
            elif new_loc_item in cost_items:
                if new_loc_item == '~':
                    if not on_land or state.has_raft:
                        costly_reachable[new_loc_item] += [path + moves + [new_direction, new_loc, state]]
                    elif no_rocks and new_loc not in not_rockable:
                        if rocks_opens_new_land(new_loc, no_rocks, is_now_land):
                            costly_reachable[new_loc_item] += [path + moves + [new_direction, new_loc, state]]
                        else:
                            not_rockable[no_rocks].add(new_loc)
                else:
                    if new_loc_item == 'T':
                        moves.insert(-1, 'c')
                    elif new_loc_item == '-':
                        moves.insert(-1, 'u')
                    costly_reachable[new_loc_item] += [path + moves + [new_direction, new_loc, state]]

    meaningless_options = defaultdict(list)
    if not reachable and costly_reachable:
        costly_reachable = sort_lists_in_dict(costly_reachable)

        #if only one option, why explore others?
        if len(costly_reachable) == 1 and cost_debth == 0:
            for item in costly_reachable:
                if len(costly_reachable[item]) == 1:
                    costly_reachable[item][0] = costly_reachable[item][0][:-3] + [cost_debth]
                    return sort_lists_in_dict(costly_reachable)

        for item in costly_reachable:
            #Hop on the boat on the nearest shore
            if item == '~' and no_rocks == 0:
                new_paths, _ = best_path_in_connected_surface(costly_reachable, item, is_now_land, water=False)
                costly_reachable['~'] = new_paths

            #Hop off the boat on the nearest shore
            if not on_land and item != '~':
                #if there are other ways off-land. Take em.
                if item == 'T' and any((item not in ('T', '~')) for item in costly_reachable):
                    continue

                #gather free path lands first
                #then we never leave in a costly way if we dont need to
                lands = []
                free_exit_items = (' ', 'O', 'a', 'k')
                costly_exit_items = ('T', 'o')
                if item in costly_exit_items:
                    for free_item in free_exit_items:
                        _, lands = best_path_in_connected_surface(costly_reachable, item, is_now_land, lands, water=False)
                if not lands:
                    lands = []

                new_paths, _ = best_path_in_connected_surface(costly_reachable, item, is_now_land, lands, water=False)
                costly_reachable[item] = new_paths

            #? was never costly, but wouldn't ditch DFS'ing into other alternatives!
            if item == '?':
                for path in costly_reachable['?']:
                    reachable[item] += [path[:-3] + [cost_debth]]
                continue

            for path in costly_reachable[item]:
                # print('Checking costly', item, 'deep path. Curr depth:', cost_debth, 
                #     'from loc: ', grid[start_loc])

                #pop those saved items off
                state = path.pop()
                new_state = deepcopy(state)
                new_loc = path.pop()
                new_direction = path.pop()

                new_state.cost_debth += 1
                pre_change_rocks = new_state.no_rocks
                new_state.add_item(item, new_loc)
                spent_rock = True if new_state.no_rocks < pre_change_rocks else False
                new_state.seen = set()

                #DFS style recursive BFS to explore deep paths
                options = very_sophisticated_bfs(maze.map, new_loc, new_state, new_direction, path)
                #options are now continuations from already established path

                if not options:
                    continue

                if 'H' in options:
                    for add_path in options['H']:
                        reachable['H'] += [path + add_path]
                        print("TAAAAAKE MEE HOOOOME COUNTRY ROOOAAAAD")
                        return sort_lists_in_dict(reachable)

                #dont pick up rock and jump sea unnecessarily
                if spent_rock:
                    sub_options = defaultdict(list)
                    for new_item in options:
                        for add_path in options[new_item]:
                            if this_rock_opens_new_land(new_loc, new_direction, add_path, state.unwalkable_tile):
                                sub_options[new_item] += [add_path]
                            else:
                                meaningless_options[new_item] += [path + add_path]
                    options = sub_options

                #dont leave rocks behind if they're better than a raft
                if item == '~' and 'o' in costly_reachable and options:
                    sub_options = defaultdict(list)
                    for new_item in options:
                        for add_path in options[new_item]:
                            if not this_rock_opens_new_land(new_loc, new_direction, add_path, state.unwalkable_tile):
                                sub_options[new_item] += [add_path]
                            else:
                                meaningless_options[new_item] += [path + add_path]
                    options = sub_options

                #Add our newfounds paths onto the old ones!
                for new_item in options:
                    for add_path in options[new_item]:
                        reachable[new_item] += [path + add_path]
    
    elif len(reachable) == 0:
        pass
        #print("Returning empty")
    else:
        pass
        #print("Returning reachable untampered.")

    if not reachable and meaningless_options and state.cost_debth == 0:
        #Should never happen, but just in case.
        return meaningless_options

    # print("cost debth:", start_state.cost_debth, "\nno-cost", start_state.no_cost_items, "\ncostly ", start_state.cost_items)        
    # print("cost debth:", cost_debth, "\nno-cost", no_cost_items, "\ncostly ", cost_items)
    # print("")
    return sort_lists_in_dict(reachable)


##################################################################
############### Discover as much as possible #####################
##################################################################

def is_unknown(y, x):
    try: return maze.map[y, x] == '?'
    except: return False

def unknown_bfs(grid, start, walking_tile, goal='?', max_distance=2):
    queue = deque([[start]])
    seen = set([start])
    while queue:
        path = queue.popleft()
        y, x = path[-1]
        for y2, x2 in ((y+1,x), (y-1,x), (y,x+1), (y,x-1)):
            if (abs(y-y2) <= max_distance and abs(x-x2) <= max_distance
                    and not x2 in map_bound and not y2 in map_bound
                    and (y2, x2) not in seen): 
                if grid[y2,x2] == goal:
                    return True
                elif grid[y2,x2] in walking_tile:
                    queue.append(path + [(y2, x2)])
            seen.add((y2, x2))
    return False

def unknown_in_reach(y, x):
    view_bound = (-2, -1, 1, 2)
    return any(is_unknown(y+i, x+j) for i in view_bound for j in view_bound
                if not abs(i) == abs(j) == 1)

##################################################################
############### Recognising islands and seas #####################
##################################################################

def best_path_in_connected_surface(costly_reachable, item, is_now_land, surfaces=[], water=False):
    new_paths = []
    surfaces = []
    for path in costly_reachable[item]:
        new_loc = path[-2]
        for surface in surfaces:
            if new_loc in surface:
                break
        else:
            surface = connected_surface(new_loc, is_now_land, water)
            surfaces += [surface]
            new_paths += [path]
    return new_paths, surfaces

def connected_surface(start, is_now_land, water=False):
    grid = maze.map
    walkable = {'~'}
    if not water:
        walkable = walkable|{'*', '.', '?'}
    timer = time.time()
    is_surface = lambda tile: ((y2, x2) not in is_now_land and grid[(y2, x2)] in walkable) == water
    queue = deque([[start]])
    seen_surface = set([start])
    seen = set([start])
    while queue:
        path = queue.popleft()
        y, x = path[-1]
        for y2, x2 in ((y+1,x), (y-1,x), (y,x+1), (y,x-1)):
            if (not x2 in map_bound and not y2 in map_bound 
                    and (y2, x2) not in seen
                    and is_surface((y2, x2))):
                queue.append(path + [(y2, x2)])
                seen_surface.add((y2, x2))
            seen.add((y2, x2))
    return seen_surface

##################################################################
############### Rock / stepping stones handling ##################
##################################################################

def is_land(loc, not_land):
    try: return not maze.map[loc] in not_land
    except: return False

def rocks_opens_new_land(loc, n, is_now_land=set()):
    #are there two separated lands around any combination of n rocks
    # this feels like it could be so much sexier

    y, x = loc
    t_tiles = set([(y-1, x), (y, x-1), (y+1, x), (y, x+1)])
    x_tiles = set([(y-1, x-1), (y+1, x+1), (y+1, x-1), (y-1, x+1)])
    walls = {'*', '.'}
    not_land = walls|sea    
    on_new_land = lambda l: (l in is_now_land or is_land(l, not_land))
    neighbour_offsets = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
    neighbours = [(y+i, x+j) for i, j in neighbour_offsets]
    landy_neighbours = [on_new_land(n_loc) for n_loc in neighbours]

    #make lands
    connected_land = [[]]
    for i in range(len(neighbours)):
        if landy_neighbours[i]:
            connected_land[-1] += [neighbours[i]]
        elif connected_land[-1]:
            connected_land += [[]]
    if landy_neighbours[0] and landy_neighbours[-1]:
        connected_land[0] += connected_land[-1]
        connected_land.pop()

    #count lands
    connected_land_count = 0
    for land in connected_land:
        if any((tile in t_tiles) for tile in land):
            connected_land_count += 1

    #more than one land connected?
    if connected_land_count == 0:
        return False
    elif connected_land_count == 1:
        new_is_now_land = is_now_land.copy()|{loc}
        for tile_loc in t_tiles:
            if maze.map[tile_loc] != '~': continue
            if n > 1 and rocks_opens_new_land(tile_loc, n-1, new_is_now_land):
                return True
        return False
    elif connected_land_count > 1:
        return True
    raise 'lol this should not happen'

def this_rock_opens_new_land(loc, direction, path, unwalkable_tile):
    if path[:2] == ['r', 'r']: return False
    move_dict = move_dict_generator[direction](*loc)
    not_new_land = {'~'}|unwalkable_tile
    for new_loc in move_dict:
        action = move_dict[new_loc][0]
        if action == path[:len(action)] and maze.map[new_loc] in not_new_land:
            return False
    return True

##################################################################
######################### ON A MISSION ###########################
##################################################################

def set_mission(target, mission):
    global maze
    maze.target = target
    maze.mission = mission[::-1] #reversed cuz .pop() is fast
    mission_to_show = mission
    if len(mission_to_show) > 4:
        mission_to_show = mission_to_show[:4] + ['... +%d' % (len(mission_to_show)-4)]
    print("SETTING MISSION to %s, %s" % (target, mission_to_show))
    return maze.mission.pop()

def bfs_mission_tactic(view):
    global maze
    if maze.mission:
        print('WERE ON A MISSION to %s' % maze.target)
        return maze.mission.pop()

    curr_state = State(maze.on_land, maze.has_gold, maze.has_key, maze.has_axe, maze.has_raft, maze.no_rocks)
    
    reachable = very_sophisticated_bfs(maze.map, tuple(maze.player), curr_state)

    # print("Options available:")
    # for item in reachable:
    #     print(item, len(reachable[item]))
    #     print(reachable[item][0])
    #     print('')

    #these actions costs us nothing to do
    for target in ('H', '$', 'k', 'a', '-', '?'):
        if target == '-' and not maze.has_key: continue
        if target in reachable:
            print("Costless mission lets go.")
            return set_mission(target, reachable[target][0][:-1])

    # actions that costs needs planning
    # DFS exploring opportuinities
    if len(reachable) == 1:
        for only_option in reachable:
            if len(reachable[only_option]) == 1:
                return set_mission(only_option, reachable[only_option][0][:-1])

    print("Tactic failed. Results below.")
    print(reachable)
    return None

##################################################################
########################### GET_ACTION ###########################
##################################################################

def print_map_around_point(loc, size=3, now_land=set()):
    y, x = loc
    landmass = lambda tile: maze.map[tile] if tile not in now_land else ' '
    for i in reversed(range(-size, size+1)):
        print('| ' + ' '.join([landmass((y-i, x-j)) for j in range(-size, size+1)])+ ' |')

rounds_played = 0
check_on_round = 200
def continue_or_na(turns=False):
    global rounds_played, check_on_round
    rounds_played += 1
    if rounds_played > check_on_round or not turns:
        while True:
            inp = input("[%d] Press Enter to continue." % check_on_round)
            inp.strip()
            inp += '.'
            if inp[0] == '.':
                if not turns:
                    check_on_round += 1
                break

def record_move(func):
    def func_wrapper(*args, **kwargs):
        move = func(*args, **kwargs)
        maze.no_moves += 1
        maze.moves += [move]
        if move == 'f': 
            maze.f()
        elif move == 'l': 
            maze.l()
        elif move == 'r':
            maze.r()
        elif move == 'c':
            maze.has_raft = True
        print('\n####  %s  ####\n' % move)
        return move
    return func_wrapper

@record_move
def get_action(view):
    global maze
    """
        Remember initial position!
        axe & key lasts forever
        rocks & rafts does NOT
        rocks are used BEFORE rafts
        only 1 raft can be held at a time
        return treasure to initial location
        ^I must remember these daaaamn
        map is n * m, n & m <= 80, starting anywhere
        max 10000 moves
    """
    """
        2 ways:
            1. generate a TONNE of maps and NN
            2. develop more manual heuristic to game

    """
    #continue_or_na(True)
    maze.add_to_map(view)
    #maze.print_map()

    #if maze.no_moves < 220:
    #try:
    move = bfs_mission_tactic(view)
    # except KeyboardInterrupt:
    #     move = False

    if move:
        print('####  BFS MISSION TACTIC  ####')
        return move
    else:
        pass
        #maze.print_map()


    # move = run_amock(view)
    # if move:
    #     print('####  RUNNING AMOCK  ####')
    #     return move

    # input loop to take input from user (only returns if this is valid)
    while 1:
        inp = input("Enter Action(s): ")
        inp.strip()
        for char in inp:
            if char in ['f','l','r','c','u','b']:
                return char
                
##################################################################
########################### PROVIDED #############################
##################################################################


# helper function to print the grid
def print_grid(view):
    #now its more square and pythonic ;)
    width = len(view[0]) * 2 - 1
    print('+' + '-'*width + '+')
    for ln in view:
        print("|%s|" % ' '.join(ln))
    print('+' + '-'*width + '+')

def main():
    # declaring visible grid to agent
    view = [['' for _ in range(5)] for _ in range(5)]
    if len(sys.argv) != 3:
        print("Usage Python3 %s -p port \n" % (sys.argv[0]))
        sys.exit(1)

    port = int(sys.argv[2])

    # checking for valid port number
    if not 1025 <= port <= 65535:
        print('Incorrect port number')
        sys.exit()

    # creates TCP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
         # tries to connect to host
         # requires host is running before agent
         sock.connect(('localhost',port))
    except (ConnectionRefusedError):
         print('Connection refused, check host is running')
         sys.exit()

    # navigates through grid with input stream of data
    j, i = 0, 0
    while True:
        data = sock.recv(100)
        if not data:
            print("not data? um you died or won.")
            exit()
        for ch in data:
            if (i == 2 and j == 2):
                view[i][j] = '^'
                view[i][j+1] = chr(ch)
                j += 1 
            else:
                view[i][j] = chr(ch)
            j += 1
            if j > 4:
                j = 0
                i = (i+1) % 5
        if j == 0 and i == 0:
            action = get_action(view) # gets new actions
            sock.send(action.encode('utf-8'))

    sock.close()

if __name__ == "__main__":
    main()